
#include "LokaLauncher.h"
#include "ModuleManager.h"
#include "LaunchEngineLoop.h"
#include "../Launch/Private/LaunchEngineLoop.cpp"
#include "Public/Async/TaskGraphInterfaces.h"
#include "SlateApplication.h"
#include "Slate/SMainWindow.h"
#include "Torrent/TestTorrent.h"
#include "RequestManager.h"
#include "Torrent/TorrentManifest.h"

#if PLATFORM_WINDOWS
#include "WindowsHWrapper.h"
#endif

IMPLEMENT_APPLICATION(LokaLauncher, "LokaLauncher")

#define LOCTEXT_NAMESPACE "LokaLauncher"

int RunLokaLauncher(const TCHAR* CommandLine)
{
	// start up the main loop
	GEngineLoop.PreInit(CommandLine);

	// Make sure all UObject classes are registered and default properties have been initialized
	ProcessNewlyLoadedUObjects();

	// Tell the module manager it may now process newly-loaded UObjects when new C++ modules are loaded
	FModuleManager::Get().StartProcessingNewlyLoadedObjects();

	// crank up a normal Slate application using the platform's standalone renderer
	FSlateApplication::InitializeAsStandaloneApplication(GetStandardStandaloneRenderer());

	

	FString SimpleTorrentError;
	TSharedPtr<FSimpleTorrent> SimpleTorrent = MakeShareable(new FSimpleTorrent());

	//if (SimpleTorrent->StartTorrent(SimpleTorrentError))
	//{
	//	
	//}

	//FPlatformProcess::Sleep(1);

	auto MyWindow = SNew(SMainWindow).DownloadPercent_Raw(SimpleTorrent.Get(), &FSimpleTorrent::GetPercent);
	FSlateApplication::Get().AddWindow(MyWindow);
	MyWindow->BringToFront(true);

	TSharedPtr<FOperationRequest> RequestManifest = MakeShareable(new FOperationRequest(nullptr, FServerHost("http://seed.lokagame.com/Update/", "Manifest"), "manifest.json", 0, false));
	RequestManifest->BindObject<FUpdateManifest>(200).AddLambda([&, t = SimpleTorrent.ToSharedRef()](const FUpdateManifest& InData)
	{
		for (auto tf_s : InData.Torrents)
		{
			FString Path = tf_s.Path;

			const int32 FoundTarget = Path.Find(TEXT("/"), ESearchCase::IgnoreCase, ESearchDir::FromEnd);
			Path = Path.Left(FoundTarget);
			t->AddGameTorrent(Path, tf_s.File);
		}
	});

	RequestManifest->GetRequest();

	// loop while the server does the rest
	while (!GIsRequestingExit)
	{
		FTaskGraphInterface::Get().ProcessThreadUntilIdle(ENamedThreads::GameThread);
		FStats::AdvanceFrame(false);
		FTicker::GetCoreTicker().Tick(FApp::GetDeltaTime());
		FSlateApplication::Get().PumpMessages();
		FSlateApplication::Get().Tick();
		FPlatformProcess::Sleep(0);
	}

	FModuleManager::Get().UnloadModulesAtShutdown();
	FSlateApplication::Shutdown();

	return 0;
}

#if PLATFORM_WINDOWS
int WINAPI WinMain(_In_ HINSTANCE hInInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR, _In_ int nCmdShow)
{
	// do the slate viewer thing
	RunLokaLauncher(GetCommandLineW());

	return 0;
}
#endif