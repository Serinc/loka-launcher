// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TorrentManifest.generated.h"

USTRUCT()
struct LOKALAUNCHER_API FUpdatePair
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FString		Path;
	UPROPERTY()		FString		File;
};

USTRUCT()
struct LOKALAUNCHER_API FUpdateManifest
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FString					GameVersion;
	UPROPERTY()		TArray<FUpdatePair>		Torrents;
};

USTRUCT()
struct LOKALAUNCHER_API FUpdateManifestWrapper
{
	GENERATED_USTRUCT_BODY();

	UPROPERTY()		FUpdateManifest			Value;
};

// http://seed.lokagame.com/Update/manifest.json