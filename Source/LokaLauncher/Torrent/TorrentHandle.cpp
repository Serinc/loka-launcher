// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaLauncher.h"
#include "TorrentHandle.h"
#include "TorrentHeader.h"

FTorrentHandle::FTorrentHandle()
{
	Handle = MakeShareable(new libtorrent::torrent_handle());
}

FTorrentHandle::FTorrentHandle(libtorrent::torrent_handle& InHandle)
{
	Handle = MakeShareable(new libtorrent::torrent_handle(InHandle));
}

FTorrentHandle::~FTorrentHandle()
{
	
}

TSharedRef<libtorrent::torrent_handle> FTorrentHandle::GetRaw() const
{
	return Handle.ToSharedRef();
}
