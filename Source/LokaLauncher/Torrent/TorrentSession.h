// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace libtorrent
{
	class session;
}

class LOKALAUNCHER_API FTorrentSession
{
public:

	FTorrentSession();
	virtual ~FTorrentSession();

	void Pause();
	void Resume();

	bool IsPaused() const;

	TSharedRef<libtorrent::session> GetRaw() const;

protected:

	TSharedPtr<libtorrent::session> Session;
};
