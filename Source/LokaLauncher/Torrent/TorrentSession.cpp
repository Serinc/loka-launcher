// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaLauncher.h"
#include "TorrentSession.h"
#include "TorrentHeader.h"

FTorrentSession::FTorrentSession()
{
	Session = MakeShareable(new libtorrent::session());
}

FTorrentSession::~FTorrentSession()
{
	if (Session.IsValid())
	{
		Session.Reset();
	}
}

void FTorrentSession::Pause()
{
	if (Session.IsValid())
	{
		Session->pause();
	}
}

void FTorrentSession::Resume()
{
	if (Session.IsValid())
	{
		Session->resume();
	}
}

bool FTorrentSession::IsPaused() const
{
	if (Session.IsValid())
	{
		return Session->is_paused();
	}

	return false;
}

TSharedRef<libtorrent::session> FTorrentSession::GetRaw() const
{
	return Session.ToSharedRef();
}
