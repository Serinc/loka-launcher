// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaLauncher.h"
#include "AddTorrentParams.h"

FAddTorrentParams::FAddTorrentParams()
{
	Params = MakeShareable(new libtorrent::add_torrent_params());
}

FAddTorrentParams::~FAddTorrentParams()
{
	
}

TSharedRef<libtorrent::add_torrent_params> FAddTorrentParams::GetRaw() const
{
	return Params.ToSharedRef();
}
