// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

namespace libtorrent
{
	struct torrent_handle;
}

class LOKALAUNCHER_API FTorrentHandle
{
public:

	FTorrentHandle();
	FTorrentHandle(libtorrent::torrent_handle& InHandle);
	virtual ~FTorrentHandle();

	TSharedRef<libtorrent::torrent_handle> GetRaw() const;

protected:

	TSharedPtr<libtorrent::torrent_handle> Handle;
};
