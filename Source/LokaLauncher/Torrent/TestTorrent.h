// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HttpRequestAdapter.h"

namespace libtorrent
{
	class session;
}

class LOKALAUNCHER_API FSimpleTorrent
{
public:

	FSimpleTorrent();
	virtual ~FSimpleTorrent();

	bool StartTorrent(FString& OutError);
	bool AddGameTorrent(const FString& InRelativePath, const FString& InTorrentUrl);

	float GetPercent() const;
	FText GetSpeedAsText() const;

protected:

	void OnDownloadFile(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool IsSuccess, const FString InPath, const FString InTorrentPath);

	TSharedPtr<libtorrent::session> Session;
};
