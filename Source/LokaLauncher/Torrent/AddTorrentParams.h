// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TorrentHeader.h"

struct LOKALAUNCHER_API FAddTorrentParams
{
public:

	FAddTorrentParams();
	virtual ~FAddTorrentParams();

	TSharedRef<libtorrent::add_torrent_params> GetRaw() const;

protected:

	TSharedPtr<libtorrent::add_torrent_params> Params;
};
