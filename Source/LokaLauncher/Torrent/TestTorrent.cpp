// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaLauncher.h"
#include "TestTorrent.h"
#include "TorrentHeader.h"
#include "HttpModule.h"
#include "FileHelper.h"
#include "IHttpResponse.h"
#include "PlatformProcess.h"

FSimpleTorrent::FSimpleTorrent()
{
	libtorrent::settings_pack sett;
	sett.set_str(libtorrent::settings_pack::listen_interfaces, "0.0.0.0:6881");

	Session = MakeShareable(new libtorrent::session(sett));
}

FSimpleTorrent::~FSimpleTorrent()
{

}

bool FSimpleTorrent::StartTorrent(FString& OutError)
{
	try
	{
		namespace lt = libtorrent;
		using namespace libtorrent;

		std::string base_path(".");



		add_torrent_params p;
		p.save_path = base_path;
		p.flags = add_torrent_params::flag_auto_managed | add_torrent_params::flag_update_subscribe;
		p.flags &= ~add_torrent_params::flag_duplicate_is_error;

		p.ti = boost::make_shared<torrent_info>("test.torrent");

		//
		Session->add_torrent(std::move(p));
		//current_handle = new lt::torrent_handle(current_session->add_torrent(p, ec));
	}
	catch(std::exception e)
	{
		
	}
	return true;
}

bool FSimpleTorrent::AddGameTorrent(const FString& InRelativePath, const FString& InTorrentUrl)
{
	FHttpModule::Get().SetHttpTimeout(60.0f);
	FHttpModule::Get().SetMaxReadBufferSize(8 * 1024 * 1024);
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();

	HttpRequest->OnProcessRequestComplete().BindRaw(this, &FSimpleTorrent::OnDownloadFile, InRelativePath, InTorrentUrl);	
	HttpRequest->SetURL("http://seed.lokagame.com/Update/" + InTorrentUrl);

	return HttpRequest->ProcessRequest();
}

void FSimpleTorrent::OnDownloadFile(FHttpRequestPtr InRequest, FHttpResponsePtr InResponse, bool IsSuccess, const FString InPath, const FString InTorrentPath)
{
	using namespace libtorrent;

	add_torrent_params p;
	p.save_path = TCHAR_TO_ANSI(*InPath);
	p.flags = add_torrent_params::flag_auto_managed | add_torrent_params::flag_update_subscribe;
	p.flags &= ~add_torrent_params::flag_duplicate_is_error;

	if (FFileHelper::SaveArrayToFile(InResponse->GetContent(), *InTorrentPath))
	{
		FPlatformProcess::Sleep(1);

		p.ti = boost::make_shared<torrent_info>(TCHAR_TO_ANSI(*InTorrentPath));

		Session->add_torrent(std::move(p));
	}
}

float FSimpleTorrent::GetPercent() const
{
	uint64 TotalSize = 0;
	uint64 DownloadedSize = 0;

	auto torrents = Session->get_torrents();
	for (auto t : torrents)
	{
		if (t.torrent_file()->is_valid())
		{
			auto files = t.torrent_file()->files();

			for (SIZE_T i = 0; i < files.num_files(); ++i)
			{
				TotalSize += files.file_size(i);
			}
		}

		std::vector<boost::int64_t> localProgress;
		t.file_progress(localProgress);

		for (auto ds : localProgress)
		{
			DownloadedSize += ds;
		}
	}

	if (TotalSize != 0 && DownloadedSize != 0)
	{
		return double(DownloadedSize) / double(TotalSize);
	}

	return 0;
}

FText FSimpleTorrent::GetSpeedAsText() const
{
	return FText::GetEmpty();
}


