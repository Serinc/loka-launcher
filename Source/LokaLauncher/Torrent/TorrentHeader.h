// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#pragma warning(disable:4582)
#pragma warning(disable:4596)

#define TORRENT_NO_DEPRECATE 1
#define BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE 1

THIRD_PARTY_INCLUDES_START
#include "libtorrent/session.hpp"
#include "libtorrent/torrent_info.hpp"
THIRD_PARTY_INCLUDES_END

