
#include "LokaLauncher.h"
#include "OperationContainer.h"
#include "HttpModule.h"
#include "Runtime/Core/Public/Misc/Base64.h"

FString const FVerb::Post = "POST";
FString const FVerb::Get = "GET";

bool IOperationContainer::SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb)
{
	return SendRequest(host, Url, Data, "", Delegate, request, Verb);
}

bool IOperationContainer::SendRequest(const FServerHost& host, const FString& Url, const FString& Data, const FString& Token, const FHttpRequestCompleteDelegate& Delegate, TWeakPtr<IHttpRequest>& request, const FString& Verb)
{
	FHttpModule::Get().SetHttpTimeout(60.0f);
	FHttpModule::Get().SetMaxReadBufferSize(8 * 1024 * 1024);
	TSharedRef<IHttpRequest> HttpRequest = FHttpModule::Get().CreateRequest();
	request = HttpRequest;

	HttpRequest->SetContentAsString(Data);
	HttpRequest->OnProcessRequestComplete() = Delegate;
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("application/json"));

	if (Token.IsEmpty() == false)
	{
		auto TokenBase = FBase64::Encode(Token);
		HttpRequest->SetHeader(TEXT("Authorization"), FString::Printf(TEXT("%s %s"), *host.Scheme, *TokenBase));
	}

	auto fullUrl = FString::Printf(TEXT("%s/%s"), *host.Host, *Url);

	HttpRequest->SetVerb(Verb);
	HttpRequest->SetURL(fullUrl);

	return HttpRequest->ProcessRequest();
}
