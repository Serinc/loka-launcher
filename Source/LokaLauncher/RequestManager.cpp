#pragma once

#include "LokaLauncher.h"
#include "RequestManager.h"

bool FBaseRequestHandler::OnDeSerializeProxy(const FString& objectStr) const
{
	LastHandleContent = objectStr;
	LastHandleDate = FDateTime::Now();
	return OnDeSerialize(objectStr);
}

FString FOperationRequest::GetItterationString() const
{
	return FString::Printf(TEXT("%d of %d)"), GetItterationNum(), GetItterationLimit());
}

uint64 FOperationRequest::GetItterationLimit() const
{
	return TimerLimit;
}

uint64 FOperationRequest::GetItterationNum() const
{
	return TimerItter;
}

bool FOperationRequest::ItterationInc()
{
	if (IsValidTimer())
	{
		TimerItter++;
		return true;
	}
	return false;
}

void FOperationRequest::ResetItterations()
{
	TimerItter = 0;
}

bool FOperationRequest::IsValidTimer() const
{
	return TimerValidate == false || TimerValidate && TimerItter < TimerLimit;
}

void FOperationRequest::OnHttpRequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) const
{
	if (HttpResponse.IsValid())
	{
		//==================================================
		const auto code = HttpResponse->GetResponseCode();
		const auto content = HttpResponse->GetContentAsString().TrimQuotes().ReplaceEscapedCharWithChar();
		//==================================================
		//UE_LOG(LogHttp, Warning, TEXT("Request: %s -> [%s][%d] %s"), *OperationName, *Token.Get().ToString(), code, *content);
		//==================================================
		if (Handlers.Contains(code) && Handlers[code].IsValid())
		{
			if (Handlers[code]->OnDeSerializeProxy(content)) { return; }
		}
		else if (code == 200)
		{
			//UE_LOG(LogHttp, Warning, TEXT("OnHttpRequest[Operation: %s][Code: %d] not bind!"), *OperationName, error.Status);
			return;
		}
	}
}

void FOperationRequest::OnTimerItterationProxy()
{
	TimerLastExecuteDate = FDateTime::Now();
}
