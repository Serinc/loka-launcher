// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SWindow.h"


/**
 * 
 */
class LOKALAUNCHER_API SMainWindow : public SWindow
{
public:
	SLATE_BEGIN_ARGS(SMainWindow)
	{}
	SLATE_ATTRIBUTE(float, DownloadPercent)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);

protected:

	TAttribute<float> DownloadPercent;
};
