// Fill out your copyright notice in the Description page of Project Settings.

#include "LokaLauncher.h"
#include "SMainWindow.h"
#include "SlateOptMacros.h"
#include "SScaleBox.h"
#include "STextBlock.h"
#include "SProgressBar.h"

#define LOCTEXT_NAMESPACE "LokaLauncher"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SMainWindow::Construct(const FArguments& InArgs)
{
	DownloadPercent = InArgs._DownloadPercent;

	auto SuperArgs = SWindow::FArguments()
		.FocusWhenFirstShown(true)
		.SupportsTransparency(FWindowTransparency(EWindowTransparency::PerPixel))
		.LayoutBorder(FMargin(0))
		.CreateTitleBar(true)
		.SizingRule(ESizingRule::FixedSize)
		.ClientSize(FVector2D(640, 480))
		[
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			+ SVerticalBox::Slot().AutoHeight()
			[
				SNew(SOverlay)
				+ SOverlay::Slot()
				[
					SNew(SProgressBar).BarFillType(EProgressBarFillType::LeftToRight).Percent_Lambda([&]() -> TOptional<float> { return DownloadPercent.Get(.0f); })
				]
				+ SOverlay::Slot().HAlign(HAlign_Center)
				[
					SNew(STextBlock).Text_Lambda([&]() -> FText { return FText::AsPercent(DownloadPercent.Get(.0f), &FNumberFormattingOptions().SetMinimumFractionalDigits(2)); })
				]
			]
		];

	SWindow::Construct(SuperArgs);
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

#undef LOCTEXT_NAMESPACE