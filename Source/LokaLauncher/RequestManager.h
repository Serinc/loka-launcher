#pragma once

#include "OperationContainer.h"
#include "JsonObjectConverter.h"


class FOperationRequest;
//=========================================================
//						[Request Handler]

class FBaseRequestHandler
{
public:
	mutable FDateTime	LastHandleDate;
	mutable FString		LastHandleContent;
	mutable const FOperationRequest* Operation;

	FBaseRequestHandler(const FOperationRequest* operation)
		: Operation(operation)
	{
		
	}

	virtual ~FBaseRequestHandler() = 0
	{
		Unbind();
	}

	bool OnDeSerializeProxy(const FString& objectStr) const;

	virtual void Unbind() = 0
	{

	}

	virtual bool OnDeSerialize(const FString& objectStr) const = 0
	{
		return false;
	}
};

class FCallBackRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE(FOnDeSerialize);

	FOnDeSerialize Handler;

	void Unbind() override
	{
		Handler.Clear();
	}

	FCallBackRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	bool OnDeSerialize(const FString& /*objectStr*/) const override
	{
		if (Handler.IsBound())
		{
			Handler.Broadcast();
		}

		return true;
	}
};

//=========================================================
//						[Object Handler]

template<class TResponseObject>
class FObjectRequestHandler : public FBaseRequestHandler
{ 
public:	
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)

	void Unbind() override
	{
		Handler.Clear();
	}

	FObjectRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	FOnDeSerialize Handler;
	bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;
		auto target = objectStr.Replace(TEXT("\\"), TEXT("/"));
		//===============================
		//static_assert(TIsClass<TResponseObject>(), "Is not child of UStruct");
		if (FJsonObjectConverter::JsonObjectStringToUStruct(target, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};

//=========================================================
//						[Array Handler]

template<class TResponseObject>
class FArrayObjectRequestHandler : public FBaseRequestHandler
{
public:
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeSerialize, const TResponseObject&)


	void Unbind() override
	{
		Handler.Clear();
	}

	FOnDeSerialize Handler;
	
	FArrayObjectRequestHandler(const FOperationRequest* const operation)
		: FBaseRequestHandler(operation)
	{

	}

	bool OnDeSerialize(const FString& objectStr) const override
	{
		TResponseObject object;

		//===============================
		static_assert(std::is_class<TResponseObject>::value, "Is not child of UStruct");
		if (FJsonObjectConverter::JsonArrayStringToUStruct(objectStr, &object, 0, 0))
		{
			//===============================
			if (Handler.IsBound())
			{
				Handler.Broadcast(object);
			}
			return true;
		}
		return false;
	}
};


class FOperationRequest
{
	//======================================
	//				Property
	TMap<int32, TSharedPtr<FBaseRequestHandler>> Handlers;
	mutable TWeakPtr<IHttpRequest>		Request;
	const	FServerHost					ServerHost;
	const	FString						OperationName;

	//======================================
	//				Timer

	mutable FDateTime					TimerStartDate;
	mutable FDateTime					TimerLastExecuteDate;

	FHttpRequestCompleteDelegate		OnRequestComplete;

	const float							TimerRate;
	const bool							TimerLoop;
	
	const bool							TimerValidate;
	const uint64						TimerLimit;
	mutable uint64						TimerItter;
	const UObject*						Context;

public:
	FString GetItterationString() const;
	uint64 GetItterationLimit() const;
	uint64 GetItterationNum() const;

	bool ItterationInc();
	void ResetItterations();

	bool IsValidTimer() const;

	void OnHttpRequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) const;

	void OnTimerItterationProxy();

public:
	static TSharedPtr<FOperationRequest> Factory
	(
		const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, const float& rate = 0
		, const bool& loop = false
	)
	{
		return TSharedPtr<FOperationRequest>(new FOperationRequest(context, server_host, operation_name, rate, loop));
	}

	static TSharedPtr<FOperationRequest> Factory
	(
		const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, const float& rate = 0
		, const bool& loop = false
		, const uint64& timerLimit = 5
		, const bool& timerValidate = false
	)
	{
		return TSharedPtr<FOperationRequest>(new FOperationRequest(context, server_host, operation_name, rate, loop, timerLimit, timerValidate));
	}

	FOperationRequest(
        const UObject* context
		, const FServerHost& server_host
		, const FString& operation_name
		, const float& rate
		, const bool& loop
		, const uint64& timerLimit = 5
		, const bool& timerValidate = false
	)
		: ServerHost(server_host)
		, OperationName(operation_name)
		, TimerRate(rate)
		, TimerLoop(loop)
		, TimerValidate(timerValidate)
		, TimerLimit(timerLimit)
		, TimerItter(0)
		, Context(context)
	{
		OnRequestComplete.BindLambda([this, &c = Context](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
		{
			FOperationRequest::OnHttpRequestComplete(HttpRequest, HttpResponse, bSucceeded);
		});
	}


public:
	~FOperationRequest() 
	{
		Handlers.Empty();
		//================================
		OnRequestComplete.Unbind();

		if (Request.IsValid())
		{
			Request.Pin()->OnProcessRequestComplete().Unbind();
			Request.Pin()->CancelRequest();
		}
	}

	//======================================
	//				Methods
private:	

	//------------------------------------------
	//	���������� ��������������� ������
	void SendRequest(const FString& content, const FString& verb) const
	{
		IOperationContainer::SendRequest(ServerHost, OperationName, content, "", OnRequestComplete, Request, verb);
	}

public:
	//------------------------------------------
	//	���������� ������ ������� ��� ����������
	void GetRequest() const
	{
		SendRequest("", FVerb::Get);
	}

	//------------------------------------------
	//	�������� ������� ������
	template<typename TRequestObject>
	void SendRequestObject(const TRequestObject& object) const
	{
		FString jsonStr;
		jsonStr.Reserve(128);
		
		static_assert(std::is_class<TRequestObject>::value, "Is not child of UStruct");
		FJsonObjectConverter::UStructToJsonObjectString(TRequestObject::StaticStruct(), &object, jsonStr, 0, 0);
		
		SendRequest(jsonStr, FVerb::Post);
	}

	//template<typename TRequestObject>
	//void GetRequestParam(const TRequestObject& object, TokenAttribRef token = "") const
	//{
	//	FString objectStr;
	//	if (FJsonObjectConverter::UStructToJsonObjectString(objectStr, &object, 0, 0))
	//	{
	//		SendRequest(objectStr, token);
	//	}
	//}


	//======================================
	//				Binding
	
	/*! ������ �������������: Request->Bind(200).AddUObject(this, &Class::Method);*/
	FCallBackRequestHandler::FOnDeSerialize& Bind(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FCallBackRequestHandler(this);
			Handlers.Add(code, TSharedPtr<FCallBackRequestHandler>(handler));
		}
		return StaticCastSharedPtr<FCallBackRequestHandler>(Handlers[code])->Handler;
	}

	/*! ������ �������������: Request->BindObject<FSquadInviteInformation>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindObject(const int32& code)
	{
		if(Handlers.Contains(code) == false)
		{
			auto handler = new FObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}

	/*! ������ �������������: Request->BindArray<TArray<FSquadInviteInformation>>(200).AddUObject(this, &Class::Method);*/
	template<typename TResponseObject>
	typename FArrayObjectRequestHandler<TResponseObject>::FOnDeSerialize& BindArray(const int32& code)
	{
		if (Handlers.Contains(code) == false)
		{
			auto handler = new FArrayObjectRequestHandler<TResponseObject>(this);
			Handlers.Add(code, TSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(handler));
		}
		return StaticCastSharedPtr<FArrayObjectRequestHandler<TResponseObject>>(Handlers[code])->Handler;
	}
};
