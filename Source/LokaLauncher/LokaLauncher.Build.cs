// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
using System.IO;
using UnrealBuildTool;

public class LokaLauncher : ModuleRules
{
	private string ModulePath
	{
		get { return ModuleDirectory; }
	}

	private string ThirdPartyPath
	{
		get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty/")); }
	}

	public LokaLauncher(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicIncludePaths.Add("Runtime/Launch/Public");

		PublicIncludePathModuleNames.AddRange(
			new string[] {
				"Launch",
				"HTTP",
				"Json",
				"JsonUtilities",
			}
		);

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"Launch",
				"HTTP",
				"Json",
				"JsonUtilities",
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"AppFramework",
				"Core",
				"Projects",
				"Slate",
				"SlateCore",
				"StandaloneRenderer",
				"SourceCodeAccess",
				"WebBrowser",
				"HTTP",
				"Json",
				"JsonUtilities",
			}
		);

		PrivateIncludePathModuleNames.AddRange(
			new string[] {
				"SlateReflector",
			}
		);

		DynamicallyLoadedModuleNames.AddRange(
			new string[] {
				"SlateReflector",
			}
		);

		PrivateIncludePaths.Add("Runtime/Launch/Private");      // For LaunchEngineLoop.cpp include

		OptimizeCode = CodeOptimization.InShippingBuildsOnly;
		MinFilesUsingPrecompiledHeaderOverride = 3;
		bFasterWithoutUnity = true;

		bEnableShadowVariableWarnings = false;
		bEnableUndefinedIdentifierWarnings = false;

		LoadLibTorrent(Target);
		LoadLibBoost(Target);
	}

	public bool LoadLibTorrent(ReadOnlyTargetRules Target)
	{
		bool isLibrarySupported = false;

		if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
		{
			isLibrarySupported = true;
			string LibraryPath = Path.Combine(ThirdPartyPath, "LibTorrent", "Lib");

			if (Target.Platform == UnrealTargetPlatform.Win64)
			{
				LibraryPath = Path.Combine(LibraryPath, "Win64");
			}
			else if (Target.Platform == UnrealTargetPlatform.Win32)
			{
				LibraryPath = Path.Combine(LibraryPath, "Win32");
			}

			PublicLibraryPaths.Add(LibraryPath);
			PublicAdditionalLibraries.Add(Path.Combine(LibraryPath, "torrent-rasterbar.lib"));

			PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "LibTorrent", "Include"));

			//AddEngineThirdPartyPrivateStaticDependencies(Target,
			//	"LibTorrent"
			//);
		}

		return isLibrarySupported;
	}

	public bool LoadLibBoost(ReadOnlyTargetRules Target)
	{
		bool isLibrarySupported = false;

		if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32))
		{
			isLibrarySupported = true;

			string BoostStartPrefix = "libboost_";
			string BoostMiddlePrefix = "-vc141-mt-";
			string BoostPostPrefix = "-1_66.lib";

			string BoostPlatform = "";
			string LibraryPath = Path.Combine(ThirdPartyPath, "Boost", "Lib");

			if (Target.Platform == UnrealTargetPlatform.Win64)
			{
				BoostPlatform = "x64";
				LibraryPath = Path.Combine(LibraryPath, "Win64");
			}
			else if (Target.Platform == UnrealTargetPlatform.Win32)
			{
				BoostPlatform = "x32";
				LibraryPath = Path.Combine(LibraryPath, "Win32");
			}

			PublicLibraryPaths.Add(LibraryPath);
			PublicAdditionalLibraries.Add(Path.Combine(LibraryPath, BoostStartPrefix + "system" + BoostMiddlePrefix + BoostPlatform + BoostPostPrefix));
			//PublicAdditionalLibraries.Add(Path.Combine(LibraryPath, "libboost_atomic-vc141-mt-x64-1_66.lib"));

			PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "Boost", "Include"));

			Definitions.Add("_CRT_SECURE_NO_WARNINGS=1");
			Definitions.Add("BOOST_DISABLE_ABI_HEADERS=1");

			bUseRTTI = true;
			bEnableExceptions = true;

			//AddEngineThirdPartyPrivateStaticDependencies(Target,
			//	"Boost"
			//);
		}

		Definitions.Add(string.Format("WITH_BOOST_BINDING={0}", isLibrarySupported ? 1 : 0));

		return isLibrarySupported;
	}
}